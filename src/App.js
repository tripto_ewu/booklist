import React, { useContext, useEffect } from 'react';
import BookContextProvider from './contexts/BookContext';
import Navbar from './components/Navbar';
import BookList from './components/BookList';
import NewBookForm from './components/NewBookForm';
import { ThemeContext } from './contexts/ThemeContext';


function App() {

  const { theme} = useContext(ThemeContext);
  

  useEffect(()=>{
    //document.body.style.backgroundColor = theme;
  })

  return (

    <BookContextProvider>
      <div className={"App-"+theme}>
        <Navbar />
        <BookList />
        <NewBookForm />
      </div>
    </BookContextProvider>

  );
}

export default App;

