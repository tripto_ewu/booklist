import React, { createContext, useState } from 'react'

export const ThemeContext = createContext();




function ThemeContextProvider(props) {

    const [theme, setTheme] = useState('purple')

    const toggleTheme =(color) => {
        setTheme(color)
        console.log(theme)
    }
    return (
        <ThemeContext.Provider value={{theme, toggleTheme}}>
            {props.children}
        </ThemeContext.Provider>
    )
}

export default ThemeContextProvider
