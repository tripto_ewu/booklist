import React, { useContext } from 'react'
import { BookContext } from '../contexts/BookContext'
import { ThemeContext } from '../contexts/ThemeContext';

function Navbar() {

    const { books } = useContext(BookContext);
    const { theme, toggleTheme } = useContext(ThemeContext);

    const selectThemeHandler = (e) =>{
        toggleTheme(e.target.value)
    }
    return (
        <div className={"navbar-"+theme}>
            <h1>Reading List</h1>
            <p>Currently you habe {books.length} book to get through...</p>
            <select onChange = {selectThemeHandler}>
                <option value="purple">Purple</option>
                <option value="pink">Pink</option>
                <option value="dark">dark</option>
            </select>
        </div>
    )
}

export default Navbar
