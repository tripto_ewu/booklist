import React, { useContext } from 'react';
import { BookContext } from '../contexts/BookContext';
import BookDetails from './BookDetails';
import { ThemeContext } from '../contexts/ThemeContext';

function BookList() {
    const { books } = useContext(BookContext);
    const {theme} = useContext(ThemeContext)
    
    return books.length ? (
        <div className={"bookList-"+theme}>
            <ul>
                {books.map(book => {
                    return (<BookDetails book={book} key={book.id} />)
                })}
            </ul>
        </div>
    ) : (
            <div className="empty">No books to read. Hello free time :)</div>
        )
}

export default BookList;
